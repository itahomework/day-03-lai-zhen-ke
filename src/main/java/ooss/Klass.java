package ooss;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Klass {

    private int number;
    private Student leader;
    private List<Person> attacherList;
    public Klass(int number) {
        this.number = number;
        attacherList = new ArrayList<>();
    }

    @Override
    public boolean equals(Object obj) {
        return this.number == ((Klass) obj).getNumber();
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }

    public int getNumber() {
        return number;
    }

    public void assignLeader(Student student) {
        if (!student.isIn(this)){
            System.out.println("It is not one of us.");
        }else {
            this.leader = student;
            for (Person attacher : attacherList){
//                if(attacher instanceof Student){
//                    System.out.printf("I am %s, student of Class %d. I know Tom become Leader.%n",attacher.getName(),this.number);
//                }
//                else if(attacher instanceof Teacher){
//                    System.out.printf("I am %s, teacher of Class %d. I know Tom become Leader.%n",attacher.getName(),this.number);
//                }
                attacher.printLeaderInfo(this);
            }
        }
    }

    public boolean isLeader(Student student) {
        if (leader != null){
            return this.leader.equals(student);
        }
        return false;
    }

    public Student getLeader() {
        return leader;
    }

    public void attach(Person person) {
        if (person instanceof Teacher){
            if (((Teacher) person).belongsTo(this)){
                attacherList.add(person);
            }
        }
        if (person instanceof Student){
            if (((Student) person).isIn(this)){
                attacherList.add(person);
            }
        }
    }
}
