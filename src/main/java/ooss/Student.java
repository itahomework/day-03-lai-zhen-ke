package ooss;

public class Student extends Person {

    private Klass klass;

    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        String introduce = super.introduce() + " I am a student.";
        if (klass != null){
            if (klass.isLeader(this)){
                introduce += " I am the leader of class " + klass.getNumber() + ".";
            }else {
                introduce += " I am in class " + klass.getNumber() + ".";
            }
        }
        return introduce;
    }

    public void join(Klass klass) {
        this.klass = klass;
    }

    public boolean isIn(Klass klass) {
        if (this.klass == null)return false;
        return this.klass.getNumber() == klass.getNumber();
    }

    public Klass getKlass() {
        return klass;
    }

    @Override
    public void printLeaderInfo(Klass klass) {
        System.out.printf("I am %s, student of Class %d. I know %s become Leader.%n", getName(), klass.getNumber(), klass.getLeader().getName());
    }
}
