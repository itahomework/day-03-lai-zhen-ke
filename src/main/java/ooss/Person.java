package ooss;

import java.util.Objects;

public class Person {
    public Person(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    private int id;
    private String name;
    private int age;

    public String introduce(){
        return String.format("My name is %s. I am %d years old.",name, age);
    }

    public void printLeaderInfo(Klass klass){

    }

    @Override
    public boolean equals(Object obj) {
        return this.id == ((Person) obj).getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
