package ooss;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Teacher extends Person {

    private List<Klass> klassList;
    public Teacher(int id, String name, int age) {
        super(id, name, age);
        klassList = new ArrayList<>();
    }

    @Override
    public String introduce() {
        if (klassList.size() == 0){
            return super.introduce() + " I am a teacher.";
        }
        StringBuilder tail = new StringBuilder(" I teach Class ");
        for (int i = 0; i < klassList.size(); i++) {
            tail.append(klassList.get(i).getNumber());
            if (i != klassList.size() - 1){
                tail.append(", ");
            }else {
                tail.append(".");
            }
        }
        return super.introduce() + " I am a teacher." + tail;
    }

    public void assignTo(Klass klass) {
        klassList.add(klass);
    }

    public boolean belongsTo(Klass klass) {
        return klassList.contains(klass);
    }

    public boolean isTeaching(Student student) {
        return belongsTo(student.getKlass());
    }

    @Override
    public void printLeaderInfo(Klass klass) {
        System.out.printf("I am %s, teacher of Class %d. I know %s become Leader.%n", getName(), klass.getNumber(), klass.getLeader().getName());
    }
}
